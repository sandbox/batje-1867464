<?php

/**
 * implements hook_form()
 *
 * This form is used to pick a date and on submit draw 1 winner for each active prize
 */
function switch_raffle_drawraffle_form($form, $form_state) {
  $form['#tree'] = TRUE;

  $form['date'] = array(
      '#type' => 'date_popup',
      '#title' => t('Day of Raffle'),
      '#required' => TRUE,
      '#date_format' => 'd/m/Y',
      '#default_value' => date( 'Y-m-d'),
    );

  // Add the submit button.
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Draw Winners'),
      '#weight' => 50,
  );

  return $form;
}

function switch_raffle_drawraffle_form_submit(&$form, &$form_state) {
  // Check if the date is a valid date and today or in the past
  // if date == empoty

  $date = $form_state['values']['date'];

  // select all prizes and amount that are active and amount > 0

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'switch_raffle_prize')
  ->propertyCondition('active', 1)
  ->propertyCondition('amount', 0, '>');

  $result = $query->execute();

  if (isset($result['switch_raffle_prize'])) {
    $ids = array_keys($result['switch_raffle_prize']);
    $prizes = entity_load('switch_raffle_prize', $ids);
  }
  else {
    drupal_set_message("There are no prizes to hand out.");
    return;
  }

  foreach ($prizes as $prize) {
    $i = 0;
    while ($i < $prize->amount) {
      if (_switch_raffle_pickwinner($date, $prize)) {
        $i++;
      }
    }
  }



}



function switch_raffle_invalidatewin_form($form, $form_state, $wid = NULL) {
  if (!isset($wid)) {
    return;
  }

  $winner = reset(entity_load('switch_raffle_winner', array($wid)));

  if (!isset($winner->wid)) {
    return;
  }

  $form['#account'] = $winner;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['wid'] = array('#type' => 'value', '#value' => $winner->wid);

  $form['sometext'] = array(
      '#type' => 'markup',
      '#prefix' => "<p>",
      '#suffix' => "</p>",
      '#markup' => t("Invalidating win " . $wid),
  );

  $form['reason'] = array(
      '#label' => 'Reason',
      '#required' => TRUE,
      '#type' => 'textfield',
  );
  // Add the submit button.
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save component'),
      '#weight' => 50,
  );

  return $form;
}

function switch_raffle_invalidatewin_form_validate($form, &$form_state) {
  if ($form_state['values']['reason'] == '') {
    form_set_error('error', t('You must provide a reason.'));
  }
}

function switch_raffle_invalidatewin_form_submit($form, &$form_state) {
  $winner = reset(entity_load('switch_raffle_winner', array($form_state['values']['wid'])));
  $winner->invalid = TRUE;
  $winner->reason = $form_state['values']['reason'];
  $winner->save();
  drupal_set_message("The prize was cancelled.");
  $form_state['redirect'] = 'admin/config/switch_raffle/winnersinvalidate';
}



function switch_raffle_collectprize_form($form, $form_state, $win = NULL) {
  if (!isset($win)) {
    return;
  }

  $form['type'] = array(
      '#type' => 'markup',
      '#prefix' => "<p>",
      '#suffix' => "</p>",
      '#markup' => "Collecting win " . $win,
  );

  // Add the submit button.
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save component'),
      '#weight' => 50,
  );

  return $form;

}

/**
 * implements hook_form()
 *
 * This form is used start emailing all winners.
 */
function switch_raffle_emailwinners_form($form, $form_state) {
  $form['#tree'] = TRUE;

  $form['type'] = array(
      '#type' => 'markup',
      '#prefix' => "<p>",
      '#suffix' => "</p>",
      '#markup' => t("This will email all current winners that are not invalid and have never received an email before"),
  );

  // Add the submit button.
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Email Winners'),
      '#weight' => 50,
  );

  return $form;

}

/*
 *  emails all winners that have never been notified
 */
function switch_raffle_emailwinners_form_submit(&$form, &$form_state) {
  $winners = entity_load('switch_raffle_winner', FALSE, array('notified' => FALSE, 'invalid'=> FALSE));
  if (count($winners) > 0) {
    $i = 0;
    foreach($winners as $winner) {
      $winuser = user_load($winner->wuid);
      _switch_raffle_email_winner($winuser);
      $winner->notified = TRUE;
      $winner->save();
      $i++;
    }
    drupal_set_message("We emailed " . $i . " winners.");
  }
  else {
    drupal_set_message("There were no more winners to notify.");
  }
}


function switch_raffle_user_collect($form, $form_state, $wid) {
  $form['#tree'] = TRUE;
  $winner = reset(entity_load('switch_raffle_winner', array($wid)));

  $form['#account'] = $winner;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['wid'] = array('#type' => 'value', '#value' => $winner->wid);

  $form['line1'] = array(
      '#type' => 'markup',
      '#prefix' => "<p>",
      '#suffix' => "</p>",
      '#markup' => t("Great, you won a prize. You do not need to do anything for now. Someone from Coca-Cola will contact you and arrange for the handout of your prize."),
  );


  $form['line2'] = array(
      '#type' => 'markup',
      '#prefix' => "<p>",
      '#suffix' => "</p>",
      '#markup' => t("Once your prize is handed to you, and you are asked to confirm, you will have to login using your email and password and confirm below that you received your prize."),
  );


  $form['line3'] = array(
      '#type' => 'markup',
      '#prefix' => "<p>",
      '#suffix' => "</p>",
      '#markup' => t("Only confirm below when you have your prize, and NEVER tell your password to anyone, also not to us."),
  );


  // Add the submit button.
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('I aknowledge I have received my prize'),
      '#weight' => 50,
  );

  return $form;

}

function switch_raffle_user_collect_submit(&$form, &$form_state) {
  $form_state['redirect'] = 'user/collect/' . $form_state['values']['wid'] . "/confirm";
}


function switch_raffle_user_collect_confirm($form, $form_state, $wid) {
  $winner = reset(entity_load('switch_raffle_winner', array($wid)));

  $form['#tree'] = TRUE;
  $form['#winner'] = $winner;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['wid'] = array('#type' => 'value', '#value' => $winner->wid);
  return confirm_form($form,
      t('Did you really receive your prize?'),
      'user',
      t('Did you really receive your prize? This action cannot be undone.'),
      t('Yes I did!'),
      t('Cancel')
  );
}

/**
 * Confirm collection of prize
 */
function switch_raffle_user_collect_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $winner = reset(entity_load('switch_raffle_winner', array($form_state['values']['wid'])));
    $winner->collected = TRUE;
    $winner->save();
    drupal_set_message("Congratulations! Enjoy your Coke Connect prize! Keep on surfing to win more prizes!");
  }
  $form_state['redirect'] = '<front>';
}



