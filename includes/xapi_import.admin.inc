<?php

/**
 * The class used for Switch Raffle Prizes entities
 */
class XapiImportData extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'xapi_import');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'xapi/import/' . $this->iid);
  }


}

class XapiImportDataController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a geonode data - we first set up the values that are specific
   * to our model schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the model.
   *
   * @return
   *   A model object with all default fields initialized.
   */
  public function create(array $values = array()) {
    global $user;
    // Add values that are specific to our Model
    $values += array(
        'iid' => '',
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
        'uid' => $user->uid,
        'name' => 'New Import',
        'query' => '',
        'nodetype' => '',
        'minx' => 0,
        'miny' => 0,
        'maxx' => 0,
        'maxy' => 0,
        'weight' => 0,
        'active' => FALSE
    );

    $model = parent::create($values);
    return $model;
  }

  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    drupal_set_message("You can not delete imports");
  }
}

/**
 * UI controller for Task Type.
 */
class XapiImportTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Xapi Imports.';
    return $items;
  }
}

function xapi_import_form($form, &$form_state, $entity) {
  if (isset($entity->is_new) && ($entity->is_new)) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Prize Name'),
      '#required' => TRUE,
    );
  }
  $types =_node_types_build()->names;
  $form['nodetype'] = array(
      '#title' => t("Node Type"),
      '#type' => "select",
      '#description' => t("The internal name of the nodetype that we will be importing"),
      '#default_value' => (isset($entity->is_new) && ($entity->is_new)) ? '' : $entity->nodetype,
      '#required' => TRUE,
      '#options' => $types,
      '#weight' => 8,
  );

  if (isset($entity->is_new) && ($entity->is_new)) {
    $form = _do_bb_magic($form);
  }
  else {
    $form = _do_bb_magic($form, $entity);
  }

  $options = array();
  $i = -20;
  while ($i < 21) {
    $options[$i] = $i;
    $i++;
  }

  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#options' => $options,
    '#default_value' => $entity->weight,
    '#required' => TRUE,
  );

  $form['active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => $entity->active,
  );
  $form['query'] = array(
      '#type' => 'textfield',
      '#title' => t('Query'),
      '#default_value' => $entity->query,
      '#required' => TRUE,
  );

  $form['xapi_import'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form('xapi_import', $entity, $form, $form_state);

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#weight' => 100,
  );

  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function xapi_import_form_submit(&$form, &$form_state) {
  $import = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.

  $extend = explode(',', $form_state['values']['center']['restrict']['restrictedExtent']);
  dpm ($extend);

  $import->minx = $extend[0];
  $import->miny = $extend[1];
  $import->maxx = $extend[2];
  $import->maxy = $extend[3];

  $import->save();
  $form_state['redirect'] = 'admin/config/xapi/import';
}

/*
 *  This shows a map where we can select an extend. its completely ripped from the openlayers_ui module
 */
function _do_bb_magic($form, $entity = NULL) {

  if (isset($entity)) {
  $defaults = array(
      'center'=> array(
          'restrict' => array(
              'restrictextent' => TRUE,
              'restrictedExtent' => $entity->minx . ',' . $entity->miny . ',' . $entity->maxx . ',' . $entity->maxy,
              ),
          'initial' => array (
          ),
        ),
      );
  }
  module_load_include('inc', 'openlayers_ui', 'includes/openlayers_ui.maps');
  openlayers_include();
  ctools_include('dependent');
  drupal_add_js(drupal_get_path('module', 'openlayers_ui') .
    '/js/openlayers_ui.maps.js');
  drupal_add_css(drupal_get_path('module', 'openlayers_ui') .
    '/openlayers_ui.css');  // Center


  // Center
  $form['center'] = array(
    '#title' => t('Center & Bounds'),
    '#description' => t('The bounding box for your query.
      <strong>Shift-drag</strong> a box on the map to set the Restricted Extent.'),
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#group' => 'ui'
  );
  $form['center']['helpmap'] = array(
    '#markup' => '<div class="form-item openlayers-center-helpmap"
      style="display:block">' .
      xapi_maps_form_center_map($defaults) . '</div>'
  );
  $form['center']['restrict'] = array(
    '#type' => 'fieldset',
    '#title' => t('Restrict Extent')
  );
  $form['center']['restrict']['restrictextent'] = array(
    '#type' => 'checkbox',
    '#title' => t('Restrict Extent'),
    '#description' => t('Setting the restricted extent of a map prevents users
      from panning the map outside a specified area. This can be set
      interactively by <strong>holding the shift key and dragging a box</strong> over the map
      above. Setting the extent does not restrict how far users can zoom out,
      so setting restricted zoom levels (via individual layer settings) is
      recommended.'),
    '#id' => 'restrictextent',
    '#default_value' => isset($defaults['center']['restrict']['restrictextent']) ?
      $defaults['center']['restrict']['restrictextent'] : '',
  );
  $form['center']['restrict']['restrictedExtent'] = array(
    '#type' => 'textfield',
    '#title' => t('Restricted Extent'),
    '#description' => t('Prevents users from panning outside of a specific bounding box'),
    '#default_value' => isset($defaults['center']['restrict']['restrictedExtent']) ?
      $defaults['center']['restrict']['restrictedExtent'] : '',
    '#attributes' => array('class' => array('openlayers-form-restrictedExtent')),
    '#size' => 25,
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('restrictextent' => array(1))
  );
   return $form;
}

/**
 * Create Centering Map
 *
 * Create map for interactive default centering
 *
 * @param $defaults
 *   Array of defults to use for the map of centering and zooming
 * @return
 *   Themed map array
 */
function xapi_maps_form_center_map($defaults = array()) {
  // Pass variables etc. to javascript
  $pass_values = array(
      'openlayersForm' => array(
          'projectionLayers' => openlayers_ui_get_projection_options(),
      ),
  );
  dpm($pass_values);
  drupal_add_js($pass_values, 'setting');

  // centerpoint & zoom of this map are overridden
  // by the mapformvalues behavior on page load.
  //
  // Note that navigation screws up the boxselect
  // behavior for getting extent.
  $centermap_def = array(
      'id' => 'openlayers-center-helpmap',
      'projection' => '900913',
      'displayProjection' => '900913',
      'default_layer' => 'mapquest_osm',
      'width' => '500px',
      'height' => '400px',
      'center' => array(
      'initial' => array(
      'centerpoint' => "0, 0",
      'zoom' => 2,
  ),
  ),
      'image_path' => drupal_get_path('module', 'openlayers') . '/themes/default_dark/img/',
      'layers' => array(
          'mapquest_osm',
      ),
  'behaviors' => array(
          //'openlayers_behavior_navigation' => array(),
          //'openlayers_behavior_dragpan' => array(),
          'openlayers_behavior_panzoombar' => array(),
          'openlayers_behavior_mapformvalues' => array(),
          'xapi_behavior_boxselect' => array(),
          'openlayers_behavior_attribution' => array(),
),
);
return openlayers_render_map_data($centermap_def);
}
