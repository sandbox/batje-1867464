<?php
/**
 * Implement hook_entity_info().
 *
 * We define the XAPI import jobs and imported entities
 */
function xapi_entity_info() {
  $return['xapi_import'] = array(
    'label' => t('Xapi Import'),
    'entity class' => 'XapiImportData',
    'controller class' => 'XapiImportDataController',
    'base table' => 'xapi_import',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'iid',
      'label' => 'iid',
    ),
    'load hook' => 'xapi_import_data_load',
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'access callback' => 'xapi_import_access',
    'module' => 'xapi',
    'view modes' => array(
      'full' => array(
        'label' => t('Import'),
        'custom settings' => FALSE,
      ),
    ),
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/config/xapi/import',
      'file' => 'includes/xapi_import.admin.inc',
      'controller class' => 'XapiImportTypeUIController',
    ),
  );
  return $return;
}

/**
 * Implements hook_entity_property_info_alter().
 */
function xapi_entity_property_info_alter(&$info) {
  $properties = &$info['xapi_import']['properties'];
  $properties['query'] = array(
      'label' => t("Query"),
      'type' => 'text',
      'description' => t("XAPI Query."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'query',
  );
  $properties['created'] = array(
      'label' => t("Date created"),
      'type' => 'date',
      'description' => t("The date the prize was posted."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'schema field' => 'created',
  );
  $properties['changed'] = array(
      'label' => t("Date changed"),
      'type' => 'date',
      'schema field' => 'changed',
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'description' => t("The date the prize was most recently updated."),
  );
  $properties['uid'] = array(
      'label' => t("Author"),
      'type' => 'user',
      'description' => t("The author of the prize."),
      'getter callback' => 'entity_metadata_field_verbatim_get',
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'uid',
  );
  $properties['name'] = array(
      'label' => t("Name"),
      'type' => 'text',
      'description' => t("The name of the prize."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'name',
  );
  $properties['nodetype'] = array(
      'label' => t("Nodetype"),
      'type' => 'text',
      'description' => t("The nodetype."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'name',
  );
  $properties['weight'] = array(
      'label' => t("Weight"),
      'type' => 'integer',
      'description' => t("The weight of the prize."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'weight',
  );
  $properties['minx'] = array(
      'label' => t("Minx"),
      'type' => 'integer',
      'description' => t("Left."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'minx',
  );
  $properties['miny'] = array(
      'label' => t("Miny"),
      'type' => 'integer',
      'description' => t("Bottom."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'miny',
  );
  $properties['maxx'] = array(
      'label' => t("Maxx"),
      'type' => 'integer',
      'description' => t("Right."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'maxx',
  );
  $properties['maxy'] = array(
      'label' => t("Maxy"),
      'type' => 'integer',
      'description' => t("Top."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'maxy',
  );
  $properties['active'] = array(
      'label' => t("Active"),
      'type' => 'integer',
      'description' => t("Is the prize still active (you can not delete or change a prize)."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'xapi import',
      'required' => TRUE,
      'schema field' => 'active',
  );
}

/**
*    Implements hook_permission()
 */
function xapi_permission() {
  $permissions = array();
  // We set up permisssions to manage entity types, manage all entities and the
  // permissions for each individual entity
  $permissions['xapi import'] = array(
      'title' => t('Manage XAPI imports'),
      'description' => t('Administer XAPI imports'),
  );
}

/**
 * Determines whether the given user has access to a Prize.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 * @param $model
 *   Optionally a model or a model type to check access for. If nothing is
 *   given, access for all models is determined.
 * @param $account
 *   The user to check for. Leave it to NULL to check for the global user.
 * @return boolean
 *   Whether access is allowed or not.
 */
function xapi_import_access($op, $type = NULL, $account = NULL, $entitytype = NULL) {
  if (user_access('xapi import', $account)) {
    return TRUE;
  }
  return FALSE;
}


function xapi_menu() {
  $items['xapi/import/%'] = array(
    'title' => 'Import OSM Data',
    'description' => t('Post Install Actions.'),
    'page callback' => 'xapi_import',
    'page arguments' => array(2),
    'access callback' => 'user_access',
    'access arguments' => array('xapi import'), // TODO Temporary, implement callback to detiremen 'sync own bbsync'
  );
  return $items;
}

function xapi_form(&$node, $form_state) {
  $type = node_get_types('type', $node);
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
  );
  $type = node_get_types('type', $node);
  $form['nodetype'] = array(
    '#title' => t("Node Type"),
    '#type' => "textfield",
    '#description' => t("The internal name of the nodetype that we will be importing"),
    '#default_value' => $node->nodetype,
    '#size' => 64,
    '#required' => TRUE,
    '#weight' => 8,
  );
  $form['minx'] = array(
    '#title' => t("Min X"),
    '#type' => "textfield",
    '#description' => t("The left X of the Bounding Box"),
    '#default_value' => $node->minx ? $node->minx : 0.2833,
    '#size' => 16,
    '#weight' => 10,
  );
  $form['miny'] = array(
    '#title' => t("Min Y"),
    '#type' => "textfield",
    '#description' => t("The bottom Y of the Bounding Box"),
    '#default_value' => $node->miny ? $node->miny : 32.5146,
    '#size' => 16,
    '#weight' => 11,
  );
  $form['maxx'] = array(
    '#title' => t("Max X"),
    '#type' => "textfield",
    '#description' => t("The right X of the Bounding Box"),
    '#default_value' => $node->maxx ? $node->maxx : 0.3655,
    '#size' => 16,
    '#weight' => 12,
  );
  $form['maxy'] = array(
    '#title' => t("Max Y"),
    '#type' => "textfield",
    '#description' => t("The top Y of the Bounding Box"),
    '#default_value' => $node->maxy ? $node->maxy : 32.6672,
    '#size' => 16,
    '#weight' => 13,
  );
  #miny 32.5146
  #maxx 0.3655
  #maxy 32.6672
  return $form;
}

function xapi_insert($node) {
  db_query("INSERT INTO {xapi_import} (nid, vid, nodetype, minx, miny, maxx, maxy) VALUES (%d, %d, %s, %n, %n, %n, %n)", $node->nid, $node->vid, $node->nodetype, $node->minx, $node->miny, $node->maxx, $node->maxy);
}

function xapi_update($node) {
  if ($node->revision) {
    xapi_insert($node);
  }
  else {
    db_query("UPDATE {xapi_import} SET nodetype = '%s', minx = '%n', miny = '%n', maxx = '%n', maxy = '%n' WHERE vid = %d", $node->nodetype, $node->minx, $node->miny, $node->maxx, $node->maxy, $node->vid);
  }
}

function xapi_delete(&$node) {
  db_query("DELETE FROM {xapi_import} WHERE nid = %d", $node->nid);
}

function xapi_load($node) {
  return db_fetch_object(db_query('SELECT minx, miny, maxx, maxy from {xapi_import} WHERE vid = %d', $node->vid));
}


function xapi_view($node, $teaser = FALSE, $page = FALSE) {
  if (!$teaser) {
    $node = node_prepare($node, $teaser);

    $button = "";
//    if (!$node->synced) {
      if (_xapi_isinternetexplorer()) {
        $button = l('<div class="form-submit">'. t('Import') . '</div>', "xapi/import/". $node->nid, Array('html'=> TRUE));
      }
      else {
        $button .= l('<button class="form-submit" type="button">'. t('Import') . '</button>', "xapi/import/". $node->nid, Array('html'=> TRUE));
      }
      $node->content['syncbutton'] = array(
        '#value' => $button,
        '#weight' => 5,
      );
//    }
    $node->content['minx'] = array(
      '#title' => t('Min X'),
      '#value' => $node->minx,
      '#weight' => 6,
    );
    return $node;
  }
}

function xapi_import($iid) {

  if (!($import = reset(entity_load('xapi_import', array($iid))))) {
    return "That is not a xapi_import";
  }



  //$bbox = "-77.041579,38.885851,-77.007247,38.900881";
    $bbox = $import->minx  . "," . $import->miny . "," . $import->maxx . "," . $import->maxy;
 // barrs in NYC $url = "http://open.mapquestapi.com/xapi/api/0.6/node[amenity=pub][bbox=". $bbox . "]";
//  $url = "http://open.mapquestapi.com/xapi/api/0.6/node". urlencode("[amenity=pub][bbox=". $bbox . "]");

    // If we replace node with way, we can also import polygons
    $url = "http://jxapi.osm.rambler.ru/xapi/api/0.6/node" . "[bbox=" . $bbox ."][" . $import->query ."]";

    ini_set("user_agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
    ini_set("max_execution_time", 0);
    ini_set("memory_limit", "10000M");


    /* let's try with simplexml */
  $xmlresult = simplexml_load_file($url);

  $errors = libxml_get_errors();
  foreach ($errors as $error) {
    /* no errors to be found that are any help*/
    print display_xml_error($error, $xml);
  }
  libxml_clear_errors();

  $result = "Results for ". l("This query", $url);

  $result .= "OSM Version: ". $xmlresult['version'] . "<br/>";

  	foreach($xmlresult->node as $osmnode) {
  	//  $result .= "id = ". $osmnode['id'];
  	  $result .= "<br/>". "id = ". $osmnode['id'];
  	  $result .= "<br/>". "uid = ". $osmnode['uid'];
  	  $result .= "<br/>". "timestamp = ". $osmnode['timestamp'];
  	  $result .= "<br/>". "lat = ". $osmnode['lat'];
  	  $result .= "<br/>". "lon = ". $osmnode['lon'];
  	  foreach($osmnode->tag as $osmtag) {
  	    $result .= "<br/>". "k = ". $osmtag['k'];
  	    $result .= "<br/>". "v = ". $osmtag['v'];
  	  }


  	  $node = new stdClass();
  	  $node->type = $import->nodetype;
  	  node_object_prepare($node);

  	  $node->title    = "Toilet created by " . $osmnode['uid'];
  	  $node->language = LANGUAGE_NONE;

  	  /*
  	   *  Let's pick the first geofield on the nodetype.. this should go on the form
  	   *   in fact, let's do this after the hackathon
  	   *     $instances = field_info_instances('questionnaire_answer');
  foreach ($instances as $instance_name => $instance) {
    if $instance == geofield do magic!;
  }

  	   */

  	  $node->field_toilet_location[LANGUAGE_NONE][0]['wkt'] = 'POINT(' .$osmnode['lon'] . ' ' . $osmnode['lat'] . ')';

  	  node_save($node);


  	}

  return $result;
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function xapi_ctools_plugin_directory($module, $plugin) {
  if ($module == 'openlayers' && !empty($plugin)) {
    return 'plugins/' . $plugin;
  }
}


/**
 * Implements hook_ctools_plugin_type().
 */
function xapi_ctools_plugin_type() {
  drupal_set_message("plugin types");
  // For backwards compatability, we allow for the use
  // of hooks to define these plugins.
  //
  // This should be removed in 7.x-3.x
  return array(
    'behaviors' => array(
      'use hooks' => TRUE,
      'classes' => array('behavior'),
    ),
  );
}


/**
 * Get all behaviors.
 *
 * @ingroup openlayers_api
 *
 * @param $reset
 *   Boolean whether to reset cache or not.
 * @return
 *   Array of behavior info.
 */
function xapi_behaviors($reset = FALSE) {
  drupal_set_message("xapi_behaviours");
  ctools_include('plugins');
  dpm(ctools_get_plugins('xapi', 'behaviors'));
  return ctools_get_plugins('xapi', 'behaviors');
}


/**
 * Implements hook_ctools_plugin_api().
 */
function xapi_ctools_plugin_api($module, $api) {
  drupal_set_message($module . '    '  .$api);
  if ($module == "openlayers") {
    switch ($api) {
      case 'openlayers_behaviors':
        return array('version' => 1);

    }
  }
}
